<?php
/**
 * The template for displaying the header
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
    	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    	<meta name="viewport" content="width=device-width, initial-scale=1" />
    	<link rel="profile" href="https://gmpg.org/xfn/11" />
    	<?php wp_head(); ?>

        <link rel="stylesheet" href="https://use.typekit.net/zhu4mig.css">
    </head>

    <body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
        <header>

        </header>
