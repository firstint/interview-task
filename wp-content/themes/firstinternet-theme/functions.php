<?php 

function enqueue_assets() {
    wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css');
}

add_action('wp_enqueue_scripts', 'enqueue_assets');
