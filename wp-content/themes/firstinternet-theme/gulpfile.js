const { gulp, src, series, parallel, dest, watch } = require('gulp');

// Plugins
const sass          = require('gulp-sass');
const notify        = require('gulp-notify');
const concat        = require('gulp-concat');
const uglify        = require('gulp-uglify');
const sourcemaps    = require('gulp-sourcemaps');
const postcss       = require('gulp-postcss');
const autoprefixer  = require('autoprefixer');
const cssnano       = require('cssnano');
const del           = require('del');

sass.compiler = require('node-sass');

// Compile SCSS. Minify and autoprefix the resulting CSS
function scssTask() {
	const processors = [
		autoprefixer,
		cssnano
	];

	return src([
		'assets/styles/**/*.scss'
	])
  .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(postcss(processors))
  .pipe(dest('dist/styles'))
  .pipe(notify({
  	message: 'SCSS compiled',
  	onLast: true
  }));
}

// Concat and minify JS
function jsTask() {
  return src([
    'assets/scripts/**/*.js'
  ])
  .pipe(concat('main.js'))
  .pipe(uglify())
  .pipe(dest('dist/scripts'))
  .pipe(notify({
      message: 'JS compiled',
      onLast: true
    })
  );
}

function watchScss() {
	watch(['assets/styles/**/*.scss'], { interval: 50 }, parallel(scssTask));
}

function watchJs() {
  watch(['assets/scripts/**/*.js'], { interval: 50 }, parallel(jsTask));
}

// Delete everything inside /dist
function clean() {
  return del('dist/**', {force:true});
}

exports.build = series(clean, parallel(scssTask, jsTask));
exports.default = series(exports.build, parallel(watchScss, watchJs));
